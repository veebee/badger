#!/bin/env node

var config = require('./app/config/badger-dev.js');

require('./app/app').run(config);
