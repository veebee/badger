var config,
    path = require('path'),
    express = require('express'),
    moment = require('moment')
    bodyParser = require('body-parser'),
    i18n = new (require('i18n-2')) ({
        locales: ['en', 'fr']
    }),
    BadgeController = require('../controllers/badgeController').BadgeController;

module.exports.init = function (app){

    config = app.get('config');

    app.locals.moment = moment;
    // parse application/x-www-form-urlencoded
    app.use(bodyParser.urlencoded({ extended: false }));
    // parse application/json
    app.use(bodyParser.json());
    // add validation methods to request
    //app.use(expressValidator());

    //require('../config/passport')(passport);
    require('../controllers/badgeController').init(app);
    require('../controllers/userController').init(app);

    // home page
    app.get('/', function (req, res, next) {

        BadgeController.getBadges(function (err, badges) {

            if (err) return next(err);
            res.render('admin', {
                title: 'Parking badge management',
                badges: badges
            });
        });
    });
};