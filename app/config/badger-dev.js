module.exports = {

    deploy: {
        ip: '127.0.0.1',
        port: 3000
    },
    mongoUrl: 'mongodb://localhost:27017/badger',
    cache: {
        // don't cache static resources in development
        maxAge: 0
    },
    i18n: {
        locales: ['en', 'fr']
    }
};
