var express = require('express'),
    ejsLocals = require('ejs-locals'),
    mongoose = require('mongoose'),
    _ = require('underscore'),
    flash = require('connect-flash'),
    routes = require('./routes');
    //passport = require('passport');

exports.run = function(config) {

    var app = express(),
        acceptConnections = false;

    app.set('config', config);

    app.engine('html', ejsLocals);
    app.set('view engine', 'html');
    app.set('views', __dirname + '/views');

    app.disable('x-powered-by');
    app.use(function (req, res, next) {
        res.header("X-powered-by", "Little pink men");
        next();
    });
    app.use(express.logger());

    app.use(express.static(__dirname + '/../public', {'maxAge': config.cache.maxAge}));

    app.use(express.cookieParser());
    app.use(express.bodyParser());
    app.use(express.methodOverride());
    app.use(express.session( {
        secret: 'thfiefhmzifmzfnzmfzuuuez',
        cookie: { maxAge: 1000 * 60 * 60 * 2}
    }));
    /*app.use(passport.initialize());
    app.use(passport.session());*/

    app.use(require('./libs/i18n')(config.i18n));

    app.locals._ = _;

    app.use(app.router);
    routes.init(app);//, passport);

    function clientErrorHandler(err, req, res, next) {
        if (req.xhr) {
            res.send(500, {
                error: "XHR error"
            });
        } else {
            next (err);
        }
    }

    function errorHandler (err, req, res, next) {
        console.log('Error: ', err);
        res.status(500);
        res.render('error');
    }

    app.use(function(req,res,next){
        if (acceptConnections){
            res.locals.app = app;
            next();
        } else {
            res.setHeader("Connection", "close");
            res.send(503, "Server is not accepting connections");
        }
    });

    app.use(errorHandler);

    app.use(function (req, res, next) {
        res.status(404).render('404', {title: '404'});
    })

    console.log("Connecting to database at " + config.mongoUrl);
    mongoose.connect(config.mongoUrl);

    var server = app.listen(config.deploy.port, config.deploy.ip, function() {
        acceptConnections = true;
        console.log('%s: Node server started on %s:%d', Date(Date.now()), config.deploy.ip, config.deploy.port);
    });

    process.on('SIGTERM', function () {
        console.log("Received kill signal (SIGTERM), shutting down gracefully.");

        acceptConnections = false;

        server.close(function(){
            console.log("Closed out remaining connections.");
            process.exit()
        });

        setTimeout(function(){
            console.error("Could not close connections in time, forcefully shutting down");
            process.exit(1);
        }, 30 * 1000);
    });

    return app;
};