var mongoose = require ('mongoose'),
    Schema = mongoose.Schema;

var BadgeSchema = new Schema ({
    "badgeId": String,
    "lastUpdate": Date,
    "lastOwner": String,
    "available": Boolean
});

/**
 * Creates a new badge
 * @param badgeId
 */
BadgeSchema.statics.createBadge = function createBadge (badgeId, cb) {

    var badge = new Badge({
        badgeId: badgeId,
        lastUpdate: Date.now(),
        available: true,
        lastOwner: ""
    });

    badge.save(function (err, badge) {

        console.log("Saved new badge: " + badge);

        if (err) return cb (err);
        return cb (null, badge);
    });
};

/**
 * Updates the status of a badge
 * @param badgeId
 * @param user the AD login of the owner
 */
BadgeSchema.statics.updateBadge = function updateBadge (badgeId, user, status, cb) {

    Badge.findOneAndUpdate ({ _id: badgeId},
        {
            $set: {
                lastOwner: user,
                available: status
            }
        }, function (err, badge) {
            if (err) return cb (err);
            return cb (null, badge);
        });
};

/**
 * Removes a badge from the Collection
 * @param badegId
 * @param cb
 */
BadgeSchema.statics.deleteBadge = function deleteBadge (badgeId, cb) {

    Badge.findOneAndRemove({
        _id: badgeId
    }, function (err) {
        if (err) return cb(err);
        return cb(null);
    });
};

var Badge = mongoose.model('Badge', BadgeSchema, 'Badges');

exports.Badge = Badge;