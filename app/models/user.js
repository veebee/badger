var mongoose = require ('mongoose'),
    Schema = mongoose.Schema;

var UserSchema = new Schema ({
    "name": String,
    "entity": String,
    "registered": Boolean
});

/**
 * Creates a new user and sets an activation code for him
 * @param name
 */
UserSchema.statics.createUser = function createUser (name, entity, cb) {

    var user = new User({
        name: name,
        entity: entity,
        registered: false
    });

    user.save(function (err, user) {
        if (err) return cb (err);
        return cb (null, user);
    });
};

/**
 * Updates a user document
 * @param userId
 * @param cb
 */
UserSchema.statics.updateUser = function registerUser (userId, entity, registered, cb) {

    User.findOneAndUpdate({_id: userId},
        {
            $set: {
                entity: entity,
                registered: registered
            }
        }, function (err, user) {
        if (err) return cb (err);
        return cb (null, user);
    });
};



var User = mongoose.model('User', UserSchema, 'Users');

exports.User = User;