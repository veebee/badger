var i18n = require('i18n-2');

/**
 * Configures and returns an i18n middleware to use for each request.
 *
 * @param options the options for the i18n-2 module
 * @returns {Function} the middleware function that handles i18n properly
 */
module.exports = function(options){

    return function initializeI18n (req, res, next){

        options.request = req;
        req.i18n = new i18n(options);

        if (res.locals) {
            i18n.registerMethods(res.locals, req)
        }

        var localeSet = false;
        res.locals.i18n = req.i18n;

        if (req.query.lang){
            // the 'lang' query param has been passed with the url
            req.i18n.setLocaleFromQuery();
            localeSet = true;
        } else if (req.cookies[req.i18n.cookiename]) {
            // there is a lang cookie from a previous response
            req.i18n.setLocaleFromCookie();
            localeSet = true;
        } else if (req.user && req.user.locale){
            // the user is logged in and has a lang preference
            req.i18n.setLocale(req.user.locale.toLowerCase());
            if (req.i18n.devMode){
                console.log("Overriding locale with the logged-in user preference: " + req.i18n.getLocale());
            }
            localeSet = true;
        }

        if (localeSet){
            // store the chosen locale in a cookie for future use
            res.cookie(req.i18n.cookiename, req.i18n.getLocale());
        } else {
            // default case: no locale was found, default to the HTTP Header 'accept-language'
            req.i18n.setLocale(req.i18n.preferredLocale());
            if (req.i18n.devMode){
                console.log("Overriding locale with the value of the 'accept-language' header: " + req.i18n.getLocale());
            }
        }

        next();
    }
}