var Badge = require('../models/badge').Badge,
    i18n = new (require('i18n-2')) ({
        locales: ['en', 'fr']
    });

exports.init = function (app) {

    app.put('/badge', createBadge);
    app.post('/badge/:badgeId', updateBadge);
    app.get('/badges', getBadges);
    app.delete('/badge/:badgeId', deleteBadge);
};

var BadgeController = {};

/**
 * Creates a new badge
 * @param req
 * @param res
 * @param next
 */
var createBadge = function (req, res, next) {

    var id = req.body.badgeId;

    BadgeController.isIdTaken(id, function (err, isTaken) {
        if (err) return next (err);
        if (isTaken) {
            res.send({
                badge: null,
                error: "Id already used"
            });
        }
        else {
            Badge.createBadge(id, function (err, badge) {
                if (err) return next (err);
                res.send({
                    badge: badge,
                    available: i18n.__('Available')
                });
            });
        }
    });
};

var deleteBadge = function (req, res, next) {

    var badgeId = req.params.badgeId;

    Badge.deleteBadge(badgeId, function (err) {
       if (err) return next(err);
       res.send(badgeId);
    });
};

/**
 * Updates a badge's status
 * If the user parameter sent in the request is equal to the owner field of the badge,
 * the badge is set on "available" ; otherwise, the badge is left on "unavailable" status but its currentOwner
 * is updated.
 *
 * @param req
 * @param res
 * @param next
 */
var updateBadge = function (req, res, next) {

    var newOwner = req.body.user,
        badgeId = req.params.badgeId,
        available = false;

    getBadge(badgeId, function (err, badge) {

        if (err) return next (err);

        if (newOwner == badge.lastOwner) {
            available = true;
        }

        Badge.updateBadge(badge._id, newOwner, available, function (err, badge) {
            res.send(badge);
        });
    });
};

var getBadges = function (req, res, next) {

    Badge.find( {}, function (err, badges) {
        if (err) return next(err);
        res.send(badges);
    });
};

/**
 * Retrieves a badge based on its Mongo ID
 * @param badgeId
 */
BadgeController.getBadge = function (badgeId, cb) {

    Badge.findById(badgeId, function (err, badge) {

        if (err) return cb (err);
        return cb (badge);
    });
};

/**
 * Checks whether a badge ID is already taken
 * @param badgeId
 * @param cb
 */
BadgeController.isIdTaken = function (badgeId, cb) {

    Badge.findOne({
        badgeId: badgeId
    }, function (err, badge) {
        if (err) return cb (err);
        return cb(null, null != badge);
    });
};

/**
 * Gets all badge documents
 * @param req
 * @param res
 * @param next
 */
BadgeController.getBadges = function (cb) {

    Badge.find( {}, function (err, badges) {
        if (err) return cb (err);
        return cb (null, badges);
    });
};

exports.BadgeController = BadgeController;