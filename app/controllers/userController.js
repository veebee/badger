var User = require('../models/user').User,
    MailerController = require('./mailerController').MailerController,
    UserController = {};

exports.init = function  (app) {

    app.put('/user', createUser);
    app.post('/user/:userId', updateUser);
    app.delete('/user/:userId', deleteUser);

};

/**
 * Creates a new user
 * @param req
 * @param res
 * @param next
 */
var createUser = function (req, res, next) {

    var userLogin = req.body.login,
        entity = req.body.entity;

    User.createUser(userLogin, entity, function (err, user) {

        if (err) return next (err);

        //Retrieve email address from LDAP
        //Send registration email, containing a QR code corresponding to the user's mongo ID

        res.send('200');
    });

};

var updateUser = function (req, res, next) {

    var userId = req.params.userId,
        entity = req.body.entity;

    getUser(userId, function (err, user) {
       //If an entity is provided, it means the update relates to the user's entity being changed
        if (null != entity) {
            User.updateUser(userId, entity, user.registered, function (err, user) {
                if (err) return next (err);
                res.send(user);
            });
        //Otherwise, it means the update relates to the user's activation
        } else {
            User.updateUser(userId, user.entity, true, function (err, user) {
                if (err) return next (err);
                res.send(user);
            });
        }
    });
    return;
};

/**
 * Registers a new user
 * @param req
 * @param res
 * @param next
 */
var registerUser = function (user, cb) {

    getUser(req.params.userId)
    User.updateUser(req.params.userId, "")
};


var getUser = function (userId, cb) {

  User.findById(userId, function (err, user) {
      if (err) return cb(err);
      return cb (user);
  });
};

/**
 * Removes a user account from the database
 * @param req
 * @param res
 * @param next
 */
var deleteUser = function (req, res, next) {


};