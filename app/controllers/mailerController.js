var async = require('async')
    , _ = require('underscore')
    , nodemailer = require('nodemailer')
    , smtpTransport = require('nodemailer-smtp-transport')
    , path = require('path')
    , EmailTemplate = require('email-templates').EmailTemplate
    , i18n = new (require('i18n-2')) ({
    locales: ['en', 'fr']
});

var transporter = nodemailer.createTransport(smtpTransport({
        host: '',
        port: 25,
        auth: {
            user: '',
            pass: ''
        }
    }
    )),
    fromMail = ' <info@>',
    MailerController = {};

MailerController.sendRegistrationMail = function (userLogin, registrationCode) {

    var templateDir = path.join(__dirname, '../templates', 'emails', locale),
        template = new EmailTemplate(templateDir);

    var fields = {
            registrationCode: registrationCode
        },
        text = '',
        html = '';

    template.render(fields, function (err, results) {
        if (err) return next(err);

        text = results.text;
        html = results.html;

        var mailOptions = {
            from: fromMail,
            to: user.email,
            subject: '',
            text: text,
            html: html
        };

        transporter.sendMail(mailOptions, function(error, info) {
            if (error) {
                console.log(error);
            }
        });
    });

};

exports.MailerController = MailerController;