#!/bin/env node

var config = require('./app/config/badger-dev');

require('./app/app').run(config);
