{
	"Badge ID": "Badge ID",
	"Status": "Status",
	"Last user": "Last user",
	"Last update": "Last update",
	"Add a badge": "Add a badge",
	"Last 3 digits of the badge ID": "Last 3 digits of the badge ID",
	"Save": "Save",
	"Reset": "Reset",
	"Available": "Available",
	"Badges management": "Badges management"
}