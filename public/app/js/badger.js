var badger = {};

badger.createQRCode = function (badgeId, elementId) {

    new QRCode(elementId, badgeId);
};

badger.showQRModal = function () {

    $('.show-qr-modal').on('click', function (e) {
        $('#badge-qr-modal').modal();
        var me = $(this);
        badger.createQRCode(me.data("badge-id"), "badge-qr-code");
        badger.showBadgeId(me.data("badge-badgeid"), "badge-id");
    });
}

badger.showBadgeId = function (badgeId, elementId) {

    $('#' + elementId).text(badgeId);
};

badger.resetQRModal = function () {

    $('#reset-create-badge-modal').on('click', function (e) {

        $('#input-badgeId').val("");
    });
};

badger.createBadge = function () {

    $('#save-badge-btn').on('click', function () {

       var badgeId = $('#input-badgeId').val(),
           url = window.location.href.split('#')[0] + "badge";

       if (badgeId.length > 0) {

           $.ajax({
               type: 'put',
               url: url,
               data: {
                   badgeId: badgeId
               },
               success: function (data) {
                   badger.addBadgeRow(data.badge, data.available);
               },
               error: {
                   //Display error message
               }
           });
       }
    });
};

badger.deleteBadge = function () {

    $('.delete-badge').on('click', function () {

        var badgeId = $(this).data('badge-id'),
            url = window.location.href.split('#')[0] + "badge/" + badgeId;

        $.ajax({
            type: 'delete',
            url: url,
            success: function (data) {
                $('#' + data).remove();
            },
            error: {
                //Display error message
            }
        });
    });
}

badger.addBadgeRow = function (badge, status) {

  var html = '<tr class="badge-row" id="' + badge._id + '">' +
      '<td><span class="show-qr-modal" data-badge-badgeid="' + badge.badgeId + '" data-badge-id="' + badge._id + '">' + badge.badgeId + '</span></td>' +
          '<td>' + status + '</td>' +
          '<td></td>' +
          '<td>' + badge.lastUpdate + '</td>' +
          '<td><i class="glyphicon glyphicon-trash" data-badge-id="' + badge._id + '"></i></td>' +
          '</tr>';

    badger.deleteBadge();
    $('#badges-summary').append(html);
};

badger.init = function () {

    badger.showQRModal();
    badger.resetQRModal();
    badger.createBadge();
    badger.deleteBadge();
}